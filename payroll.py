# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import PoolMeta, Pool


class Payroll(metaclass=PoolMeta):
    __name__ = "staff.payroll"

    # def set_preliquidation(self, extras, discounts=None):
    #     super(Payroll, self).set_preliquidation(extras, discounts)
    #     Access = Pool().get('staff.access')
    #     PayrollLine = Pool().get('staff.payroll.line')
    #     config = Pool().get('surveillance.configuration')(1)
    #     if config and config.wage_type_extra_compensation:
    #         start = datetime.combine(self.start, time(0, 0, 0))
    #         end = datetime.combine(self.end, time(23, 59, 59))
    #         wt_extra = config.wage_type_extra_compensation
    #         if config.pay_method_shift == 'fixed_amount':
    #             accesses = Access.search([
    #                 ('employee', '=', self.employee.id),
    #                 ('enter_timestamp', '>=', start),
    #                 ('enter_timestamp', '<=', end),
    #                 ('amount_shift', '>', 0),
    #                 ('state', '!=', 'open'),
    #             ])
    #             salary = 0
    #             for line in self.lines:
    #                 if line.wage_type.id == config.wage_type_salary.id:
    #                     salary = line.amount
    #             unit_value = sum([ac.amount_shift for ac in accesses]) - salary
    #             if unit_value > 0:
    #                 value = self.get_line(wt_extra, 1, unit_value)
    #                 PayrollLine.create([value])
    #         elif config.pay_method_shift == 'extratime' and extras:
    #             self._create_line_for_hours(wt_extra, extras)

    def _create_line_for_hours(self, wt_extra, extras):
        Wage = Pool().get('staff.wage_type')
        wages = Wage.search([
            ('type_concept', '=', 'extras'),
            ('name', 'in', extras.keys())
        ])
        values = []
        # FIXME
        discount = 0
        for wage in wages:
            unit_value = wage.compute_unit_price({'salary': 0})
            qty = self.get_line_quantity(wage, self.start, self.end,
                extras, discount)
            values.append(unit_value * qty)
        if values:
            Pool().get('staff.payroll.line').create(self.get_line(
                wt_extra, 1, sum(values)))

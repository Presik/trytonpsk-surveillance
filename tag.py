# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields


class Tag(ModelSQL, ModelView):
    "Surveillance Tag"
    __name__ = "surveillance.tag"
    name = fields.Char('Name', required=True)
    code = fields.Char('Code')
    weight = fields.Float('Weight')
